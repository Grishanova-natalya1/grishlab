(function( $ ) {
 $.fn.myPlugin = function() {
 
var container = $('#container');
var ptic = $('#ptic');
var pole = $('.pole');
var pole1 = $('#pole1');
var pole2 = $('#pole2');
var score = $('#score');
var rest = $('#rest');
var speed_s = $('#speed');

var container_width = parseInt(container.width());
var container_height = parseInt(container.height());
var pole_position = parseInt(pole.css('right'));
var pole_height = parseInt(pole.css('height'));
var ptic_left = parseInt(ptic.css('left'));
var ptic_height = parseInt(ptic.height());
var speed = 10;

var upp = false;
var score_updated = false;
var game_over = false;

var game = setInterval(function(){
if(collision(ptic,pole1) || collision(ptic,pole2) || parseInt(ptic.css('top')) <= 0 || parseInt(ptic.css('top')) > container_height - ptic_height){
stopgame();
}else{
var pole_current_position = parseInt(pole.css('right'));
if(pole_current_position > container_width - ptic_left){
if(score_updated == false){
score.text(parseInt(score.text())+1);
score_updated = true;
	}
}
if(pole_current_position > container_width){
var new_height = parseInt(Math.random() * 100);
pole1.css('height',pole_height+new_height);
pole2.css('height',pole_height - new_height);
speed = speed +0,1;
score_updated = false;
pole_current_position = pole_position;
}
pole.css('right',pole_current_position + speed);
if(upp == false){
go_down();
}
}
},40);

function stopgame(){
clearInterval(game);
game_over = true;
rest.slideDown();
}

rest.click(function(){
location.reload();
});

function go_down(){
ptic.css('top',parseInt(ptic.css('top')) +2);
}

function up(){
ptic.css('top',parseInt(ptic.css('top')) - 10);
}

$(document).on('keydown',function(e){
var key = e.keyCode;
if(key === 32 && upp ===false && game_over ===false){
upp = setInterval(up,50);
	}
});
$(document).on('keyup',function(e){
var key = e.keyCode;
if(key === 32){
clearInterval(upp);
upp = false;
}	
	});
function collision($coll1,$coll2){
var x1,y1,h1,w1,b1,r1,x2,y2,h2,w2,b2,r2;
x1 = $coll1.offset().left;
y1 = $coll1.offset().top;
h1 = $coll1.outerHeight(true);
w1 = $coll1.outerWidth(true);
b1 = y1 + h1;
r1 = x1 + w1;
x2 = $coll2.offset().left;
y2 = $coll2.offset().top;
h2 = $coll2.outerHeight(true);
w2 = $coll2.outerWidth(true);
b2 = y2 + h2;
r2 = x2 + w2;
if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) 
return false;
return true;
	}
       };
})(jQuery);