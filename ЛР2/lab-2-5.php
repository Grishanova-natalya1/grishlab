<p> Гришанова Н.А. ЭАС-310 Вариант 8 и 13
<p>
<?php
echo ("Вариант 8");
print('<br>');
$a=rand(-50,50);
$b=rand(-50,50);
print("a = $a; b = $b" . '<br>');
print('<br>');
$z = fun($a+(1/$b),(pow($b,8))/(pow($a,6)) + fun(pow($a,3/4)+pow($b,5/6),$b-$a));
print("z = fun(a+1/b,b^8/a^6) + fun(a^3/4+b^5/6,b-a) = $z" . '<br>');
function fun($u, $t){
    if($u>=0 and $t>=0){
        $f = ($u/$t)-(pow($t,2)*$u);
        return $f;
    }
    elseif ($u<0 and $t>=0){
        $f = $u+(pow($t,2)/$u);
        return $f;
    }
    elseif ($u>=0 and $t<0){
        $f = $u-$t;
        return $f;
    }
	elseif ($u<0 and $t<0){
        $f = ($t+3*$u)/$u*$t;
        return $f;
    }
}
echo ("Вариант 13");
print('<br>');
$a=rand(-50,50);
$b=rand(-50,50);
print("a = $a; b = $b" . '<br>');
print('<br>');
$z = cos(pow(func($a,$b),3)) + func(($a+$b),($a-$b));
print("z = cos^3(fun(a,b)) + fun(a+b,a-b) = $z" . '<br>');
function func($u, $t){
    if(($u*$t)<(1/2)){
        $f = (1+cos($t-$u))/(($u/$t)+pow($t,2));
        return $f;
    }
    elseif (($u*$t)>=(1/2)){
        $f = sin(log(abs($u/$t)));
        return $f;
    }  
}
?>